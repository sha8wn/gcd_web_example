//
//  ViewController.h
//  GCD_Example
//
//  Created by Shawn Frank on 2/8/18.
//  Copyright © 2018 Shawn Frank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController<WKNavigationDelegate,WKUIDelegate>

@property(strong,nonatomic) WKWebView *webView;
@property (strong, nonatomic) NSString *productURL;

@end

