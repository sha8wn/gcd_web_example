//
//  ViewController.m
//  GCD_Example
//
//  Created by Shawn Frank on 2/8/18.
//  Copyright © 2018 Shawn Frank. All rights reserved.
//

#import "ViewController.h"
#import "GCDWebServer.h"
#import "GCDWebServerURLEncodedFormRequest.h"
#import "GCDWebServerDataResponse.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString* documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    /*_webServer = [[GCDWebUploader alloc] initWithUploadDirectory:documentsPath];
     _webServer.delegate = self;
     _webServer.allowHiddenItems = YES;
     if ([_webServer start]) {
     _label.text = [NSString stringWithFormat:NSLocalizedString(@"GCDWebServer running locally on port %i", nil), (int)_webServer.port];
     } else {
     _label.text = NSLocalizedString(@"GCDWebServer not running!", nil);
     }*/
    
    // file URL in our bundle
    NSURL *fileFromBundle = [[NSBundle mainBundle]URLForResource:@"bundle" withExtension:@"js"];
    
    // Destination URL
    NSArray *paths = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentsURL = [paths lastObject];
    NSURL *destinationURL = [documentsURL URLByAppendingPathComponent:@"bundle.js"];
    
    NSError *anError = nil;
    
    // copy it over
    [[NSFileManager defaultManager]copyItemAtURL:fileFromBundle toURL:destinationURL error:&anError];
    
    fileFromBundle = [[NSBundle mainBundle]URLForResource:@"index" withExtension:@"html"];
    destinationURL = [documentsURL URLByAppendingPathComponent:@"index.html"];
    [[NSFileManager defaultManager]copyItemAtURL:fileFromBundle toURL:destinationURL error:&anError];
    
    if(anError)
    {
        NSLog(@"%@",anError.description);
    }
    
    GCDWebServer* webServer = [[GCDWebServer alloc] init];
    [webServer addGETHandlerForBasePath:@"/" directoryPath:documentsPath indexFilename:@"index" cacheAge:0 allowRangeRequests:YES];
    
    [webServer addHandlerForMethod:@"GET"
                              path:@"/params"
                      requestClass:[GCDWebServerURLEncodedFormRequest class]
                      processBlock:^GCDWebServerResponse *(GCDWebServerRequest* aRequest) {
                          // TODO return json with params
                          
                          return nil;
                          
                      }];
    
    [webServer addHandlerForMethod:@"GET"
                              path:@"/appdata"
                      requestClass:[GCDWebServerURLEncodedFormRequest class]
                      processBlock:^GCDWebServerResponse *(GCDWebServerRequest* aRequest) {
                          // TODO return json with app data
                          
                          return nil;
                          
                      }];
    
    [webServer addHandlerForMethod:@"GET"
                              path:@"/output"
                      requestClass:[GCDWebServerURLEncodedFormRequest class]
                      processBlock:^GCDWebServerResponse *(GCDWebServerRequest* aRequest) {
                          // return what was saved in the output
                          // defaut data format is json
                          
                          return nil;
                          
                      }];
    
    [webServer addHandlerForMethod:@"POST"
                              path:@"/output"
                      requestClass:[GCDWebServerURLEncodedFormRequest class]
                      processBlock:^GCDWebServerResponse *(GCDWebServerRequest* aRequest) {
                          
                          // TODO save request body to some shared with ios app location
                          // return what what saved
                          // default data format is json
                          
                          NSLog(@"REACHED");
                          return nil;
                          
                      }];
    
    [webServer startWithPort:8080 bonjourName:nil];
    
    self.productURL = @"http://127.0.0.1:8080/index.html";
    NSURL *url = [NSURL URLWithString:self.productURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    _webView = [[WKWebView alloc]init] ;
    _webView.UIDelegate = self;
    _webView.navigationDelegate = self;
    [_webView loadRequest:request];
    
    _webView.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [self.view addSubview:_webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
