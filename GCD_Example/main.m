//
//  main.m
//  GCD_Example
//
//  Created by Shawn Frank on 2/8/18.
//  Copyright © 2018 Shawn Frank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
