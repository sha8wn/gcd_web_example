//
//  AppDelegate.h
//  GCD_Example
//
//  Created by Shawn Frank on 2/8/18.
//  Copyright © 2018 Shawn Frank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

